<?xml version='1.0' encoding='UTF-8'?><!-- -*- indent-tabs-mode: nil -*- -->
<!--
This program is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation; either version 2 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
details.

You should have received a copy of the GNU Lesser General Public License
along with this program; see the file COPYING.LGPL.  If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
02111-1307, USA.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:exsl="http://exslt.org/common"
                xmlns:cache="http://projectmallard.org/cache/1.0/"
                xmlns:mal="http://projectmallard.org/1.0/"
                exclude-result-prefixes="exsl mal"
                version="1.0">

<xsl:import href="/usr/share/yelp-xsl/xslt/common/color.xsl"/>
<xsl:import href="/usr/share/yelp-xsl/xslt/common/utils.xsl"/>
<xsl:import href="/usr/share/yelp-xsl/xslt/mallard/common/mal-link.xsl"/>
<xsl:import href="/usr/share/yelp-xsl/xslt/mallard/common/mal-sort.xsl"/>

<xsl:include href="mal2latex-block.xsl"/>
<xsl:include href="mal2latex-inline.xsl"/>
<xsl:include href="mal2latex-table.xsl"/>

<xsl:output method="text"/>

<xsl:param name="mal.cache" select="/cache:cache"/>

<xsl:template name="hex2dec">
  <xsl:param name="hex"/>
  <xsl:variable name="l">
    <xsl:choose>
      <xsl:when test="contains('0123456789', substring($hex, 1, 1))">
        <xsl:value-of select="number(substring($hex, 1, 1))"/>
      </xsl:when>
      <xsl:when test="contains('Aa', substring($hex, 1, 1))">
        <xsl:value-of select="10"/>
      </xsl:when>
      <xsl:when test="contains('Bb', substring($hex, 1, 1))">
        <xsl:value-of select="11"/>
      </xsl:when>
      <xsl:when test="contains('Cc', substring($hex, 1, 1))">
        <xsl:value-of select="12"/>
      </xsl:when>
      <xsl:when test="contains('Dd', substring($hex, 1, 1))">
        <xsl:value-of select="13"/>
      </xsl:when>
      <xsl:when test="contains('Ee', substring($hex, 1, 1))">
        <xsl:value-of select="14"/>
      </xsl:when>
      <xsl:when test="contains('Ff', substring($hex, 1, 1))">
        <xsl:value-of select="15"/>
      </xsl:when>
    </xsl:choose>
  </xsl:variable>
  <xsl:variable name="r">
    <xsl:choose>
      <xsl:when test="contains('0123456789', substring($hex, 2, 1))">
        <xsl:value-of select="number(substring($hex, 2, 1))"/>
      </xsl:when>
      <xsl:when test="contains('Aa', substring($hex, 2, 1))">
        <xsl:value-of select="10"/>
      </xsl:when>
      <xsl:when test="contains('Bb', substring($hex, 2, 1))">
        <xsl:value-of select="11"/>
      </xsl:when>
      <xsl:when test="contains('Cc', substring($hex, 2, 1))">
        <xsl:value-of select="12"/>
      </xsl:when>
      <xsl:when test="contains('Dd', substring($hex, 2, 1))">
        <xsl:value-of select="13"/>
      </xsl:when>
      <xsl:when test="contains('Ee', substring($hex, 2, 1))">
        <xsl:value-of select="14"/>
      </xsl:when>
      <xsl:when test="contains('Ff', substring($hex, 2, 1))">
        <xsl:value-of select="15"/>
      </xsl:when>
    </xsl:choose>
  </xsl:variable>
  <xsl:value-of select="(16 * $l) + $r"/>
</xsl:template>

<xsl:template name="convert.color">
  <xsl:param name="color"/>
  <xsl:call-template name="hex2dec">
    <xsl:with-param name="hex" select="substring($color, 2, 2)"/>
  </xsl:call-template>
  <xsl:text>,</xsl:text>
  <xsl:call-template name="hex2dec">
    <xsl:with-param name="hex" select="substring($color, 4, 2)"/>
  </xsl:call-template>
  <xsl:text>,</xsl:text>
  <xsl:call-template name="hex2dec">
    <xsl:with-param name="hex" select="substring($color, 6, 2)"/>
  </xsl:call-template>
</xsl:template>

<xsl:variable name="blue_border">
  <xsl:call-template name="convert.color">
    <xsl:with-param name="color" select="$color.blue_border"/>
  </xsl:call-template>
</xsl:variable>

<xsl:variable name="gray_background">
  <xsl:call-template name="convert.color">
    <xsl:with-param name="color" select="$color.gray_background"/>
  </xsl:call-template>
</xsl:variable>

<xsl:variable name="yellow_background">
  <xsl:call-template name="convert.color">
    <xsl:with-param name="color" select="$color.yellow_background"/>
  </xsl:call-template>
</xsl:variable>

<xsl:variable name="yellow_border">
  <xsl:call-template name="convert.color">
    <xsl:with-param name="color" select="$color.yellow_border"/>
  </xsl:call-template>
</xsl:variable>

<xsl:template name="sty">
<xsl:text>
\usepackage[top=3cm,bottom=3cm,left=2.5cm,right=2.5cm]{geometry}
\usepackage{ucs}
\usepackage{fullpage}
\usepackage{float}
\usepackage[french]{babel}
\usepackage[utf8x]{inputenc}
\PrerenderUnicode{éàêë}
\usepackage{graphicx}
\usepackage{bookman}
\usepackage{alltt}
\usepackage{boxedminipage}
\usepackage{color}
\usepackage{caption}
\definecolor{blueborder}{RGB}{</xsl:text><xsl:value-of select="$blue_border"/><xsl:text>}
\definecolor{grayback}{RGB}{</xsl:text><xsl:value-of select="$gray_background"/><xsl:text>}
\definecolor{yellowback}{RGB}{</xsl:text><xsl:value-of select="$yellow_background"/><xsl:text>}
\definecolor{yellowborder}{RGB}{</xsl:text><xsl:value-of select="$yellow_border"/><xsl:text>}

\definecolor{eopink}{rgb}{0.92,0.0,0.55}
\definecolor{eoblue}{rgb}{0.22,0.65,0.86}

\setlength{\parindent}{0in}
\setlength{\parskip}{1em}
\newenvironment{malexample}%
{\hskip -10pt \vrule width 2pt \hskip 8pt%
\begin{minipage}{\textwidth}%
\setlength{\parskip}{1em}}%
{\end{minipage}%
\par}
\newsavebox{\malnotecontainer}
\newenvironment{malnote}{%
\begin{lrbox}{\malnotecontainer}%
\begin{minipage}{\textwidth}}%
{\end{minipage}%
\end{lrbox}%
\fcolorbox{yellowborder}{yellowback}{\usebox{\malnotecontainer}}\par}
\newsavebox{\malscreencontainer}
\newenvironment{malscreen}{%
\begin{lrbox}{\malscreencontainer}%
\begin{minipage}{\textwidth}%
\begin{alltt}}%
{\end{alltt}%
\end{minipage}%
\end{lrbox}%
\fcolorbox{grayback}{grayback}{\usebox{\malscreencontainer}}\par}
\newsavebox{\malstepscontainer}
\newenvironment{malsteps}{%
\begin{lrbox}{\malstepscontainer}%
\begin{minipage}{\textwidth}%
\begin{enumerate}}%
{\end{enumerate}%
\end{minipage}%
\end{lrbox}%
\fcolorbox{blueborder}{yellowback}{\usebox{\malstepscontainer}}\vspace{0.2cm}\par}

\setlength\parindent{0pt}
\setlength{\parskip}{1ex plus 0.5ex minus 0.2ex}
\setlength\abovecaptionskip{0.1ex}

\makeatletter
\renewcommand{\maketitle}{\begin{titlepage}%%
    \let\footnotesize\small
    \let\footnoterule\relax
    \parindent \z@
    \reset@font
    \null\vfil
    \begin{flushleft}
      \color{eopink} \huge\bfseries \@title
    \end{flushleft}
    \par
    \hrule height 1pt
    \par
    \begin{flushright}
      \color{eopink} \LARGE \@author \par
    \end{flushright}
    \vskip 60\p@
    \vfil\null

    \vfill
    \vfill

    \begin{center}
    \includegraphics{entrouvert-logo}
    \end{center}

  \end{titlepage}%%
  \setcounter{footnote}{0}%%
}

\usepackage{ifthen}
\usepackage{marginnote}

\reversemarginpar

\setlength{\marginparwidth}{30pt}
\newenvironment{notice}[1]{
  \vspace{0.3cm}
  \ifthenelse{\equal{#1}{important}}{\marginpar{\vskip 3pt \includegraphics{warning}}}{}
  \ifthenelse{\equal{#1}{warning}}{\marginpar{\vskip 3pt \includegraphics{warning}}}{}
  \ifthenelse{\equal{#1}{bug}}{\marginpar{\vskip 3pt \includegraphics{warning}}}{}
  \ifthenelse{\equal{#1}{tip}}{\marginpar{\vskip 3pt \includegraphics{tip}}}{}
  \ifthenelse{\equal{#1}{advanced}}{\marginpar{\vskip 3pt \includegraphics{note}}}{}
  \ifthenelse{\equal{#1}{}}{\marginpar{\vskip 3pt \includegraphics{note}}}{}
  \hspace{0.5cm}\begin{minipage}{0.9\linewidth}
}
{
  \vspace{0.3cm}
  \end{minipage}
}

\usepackage{titlesec}
\usepackage{fancyhdr}
\usepackage{lastpage}

\pagestyle{fancy}
\addtocontents{toc}{\protect\thispagestyle{fancy}}
\fancyhead{}
\fancyfoot{}
\fancyfoot[RO, LE] {Page \thepage sur \pageref*{LastPage}}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}

\titleformat{\section}[block]{\color{eoblue}\LARGE\bfseries}{\thesection}{1em}{}
\titleformat{\subsection}[hang]{\color{eoblue}\Large\bfseries}{\thesubsection}{1em}{}
\titleformat{\subsubsection}[hang]{\color{eoblue}\large\bfseries}{\thesubsubsection}{1em}{}

\titleformat{\chapter}[block]
  {}
  {\llap{\color{eopink}\chapterNumber\thechapter
   \hspace{10pt}\vline}}
  {10pt}
  {\color{eopink}\formatchaptertitle}

\newcommand{\formatchaptertitle}[1]{%
  \parbox[t]{\dimexpr\textwidth-10pt}{\raggedright\Huge\bfseries#1}}

\newcommand{\chapterNumber}{%
  \fontsize{50}{50}\usefont{U}{eur}{b}{n}}

\renewcommand\labelitemi{$\Rightarrow$}

\usepackage{enumitem}
\setlist[itemize]{label=$\circ$}

\makeatother

</xsl:text>
</xsl:template>

<xsl:template match="/cache:cache">
  <xsl:variable name="sorted">
    <xsl:call-template name="mal.sort.tsort"/>
  </xsl:variable>
  <xsl:for-each select="exsl:node-set($sorted)/mal:link">
    <xsl:variable name="depth" select="@depth"/>
    <xsl:variable name="linkid">
      <xsl:call-template name="mal.link.xref.linkid"/>
    </xsl:variable>
    <xsl:for-each select="$mal.cache">
      <xsl:if test="$depth = 0">
        <xsl:variable name="title" select="document(key('mal.cache.key', $linkid)/@cache:href, /)/mal:page/mal:title"/>
        <xsl:variable name="author">
          <xsl:for-each select="document(key('mal.cache.key', $linkid)/@cache:href, /)/mal:page/mal:info/mal:credit[@type='author']">
            <xsl:value-of select="mal:name"/>
            <xsl:if test="not(position()=last())" >, </xsl:if>
          </xsl:for-each>
        </xsl:variable>
        <xsl:text>\documentclass[a4paper,11pt]{report}&#x000A;</xsl:text>
        <xsl:call-template name="sty"/>
        <xsl:text>\usepackage[colorlinks=true,linkcolor=eoblue,pdftitle={</xsl:text>
        <xsl:value-of select="$title"/>
        <xsl:text>},pdfauthor={</xsl:text>
        <xsl:value-of select="$author"/>
        <xsl:text>}]{hyperref}&#x000A;</xsl:text>

        <xsl:text>\title{</xsl:text><xsl:value-of select="$title"/><xsl:text>}&#x000A;</xsl:text>
        <xsl:text>\author{</xsl:text><xsl:value-of select="$author"/><xsl:text>}&#x000A;</xsl:text>
        <xsl:text>\begin{document}&#x000A;</xsl:text>
        <xsl:text>\maketitle&#x000A;</xsl:text>
        <xsl:text>\tableofcontents&#x000A;</xsl:text>
        <xsl:text>\newpage&#x000A;</xsl:text>
      </xsl:if>
      <xsl:apply-templates select="document(key('mal.cache.key', $linkid)/@cache:href, /)/mal:page">
        <xsl:with-param name="depth" select="$depth"/>
      </xsl:apply-templates>
    </xsl:for-each>
  </xsl:for-each>
  <xsl:text>&#x000A;\end{document}&#x000A;</xsl:text>
</xsl:template>

<xsl:template match="mal:page">
  <xsl:param name="depth">-1</xsl:param>
  <xsl:choose>
    <xsl:when test="$depth = 0"><xsl:text>%{</xsl:text></xsl:when>
    <xsl:when test="$depth = 1"><xsl:text>\chapter{</xsl:text></xsl:when>
    <xsl:when test="$depth = 2"><xsl:text>\section{</xsl:text></xsl:when>
    <xsl:when test="$depth = 3"><xsl:text>\subsection{</xsl:text></xsl:when>
    <xsl:when test="$depth = 4"><xsl:text>\subsubsection{</xsl:text></xsl:when>
    <xsl:when test="$depth = 5"><xsl:text>\paragraph{</xsl:text></xsl:when>
    <xsl:otherwise><xsl:text>\textbf{</xsl:text></xsl:otherwise>
  </xsl:choose>
  <xsl:value-of select="mal:title"/>
  <xsl:text>}&#x000A;</xsl:text>
  <xsl:text>\label{</xsl:text>
  <xsl:value-of select="@id"/>
  <xsl:text>}&#x000A;</xsl:text>
  <xsl:apply-templates mode="mal2latex.block.mode"
                       select="*[not(self::mal:info or self::mal:title or self::mal:subtitle or self::mal:section)]"/>
  <xsl:apply-templates select="mal:section">
    <xsl:with-param name="depth" select="$depth"/>
  </xsl:apply-templates>
</xsl:template>

<xsl:template match="mal:section">
  <xsl:param name="depth">-1</xsl:param>
  <xsl:choose>
    <xsl:when test="$depth+count(ancestor::mal:section) = 0"><xsl:text>\chapter{</xsl:text></xsl:when>
    <xsl:when test="$depth+count(ancestor::mal:section) = 1"><xsl:text>\section{</xsl:text></xsl:when>
    <xsl:when test="$depth+count(ancestor::mal:section) = 2"><xsl:text>\subsection{</xsl:text></xsl:when>
    <xsl:when test="$depth+count(ancestor::mal:section) = 3"><xsl:text>\subsubsection{</xsl:text></xsl:when>
    <xsl:when test="$depth+count(ancestor::mal:section) = 4"><xsl:text>\paragraph{</xsl:text></xsl:when>
    <xsl:otherwise><xsl:text>\textsc{</xsl:text></xsl:otherwise>
  </xsl:choose>
  <xsl:value-of select="mal:title"/>
  <xsl:text>}&#x000A;</xsl:text>
  <xsl:apply-templates mode="mal2latex.block.mode"
                       select="*[not(self::mal:info or self::mal:title or self::mal:subtitle or self::mal:section)]"/>
  <xsl:apply-templates select="mal:section">
    <xsl:with-param name="depth" select="$depth"/>
  </xsl:apply-templates>
</xsl:template>

<xsl:template name="mal.sort.tsort">
  <xsl:param name="node" select="key('mal.cache.key', $mal.link.default_root)"/>
  <xsl:variable name="sorted">
    <xsl:call-template name="_mal.sort.tsort.node">
      <xsl:with-param name="node" select="$node"/>
    </xsl:call-template>
  </xsl:variable>
  <xsl:variable name="nodes" select="exsl:node-set($sorted)/mal:link"/>
  <xsl:for-each select="$nodes">
    <xsl:variable name="xref" select="@xref"/>
    <xsl:if test="not(preceding::*[string(@xref) = $xref]) and not(contains($xref, '#'))">
      <xsl:copy-of select="."/>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<!--#* _mal.sort.tsort.node -->
<xsl:template name="_mal.sort.tsort.node">
  <xsl:param name="node" select="key('mal.cache.key', $mal.link.default_root)"/>
  <xsl:param name="depth" select="0"/>
  <xsl:param name="done" select="''"/>
  <xsl:variable name="linkid">
    <xsl:call-template name="mal.link.linkid">
      <xsl:with-param name="node" select="$node"/>
    </xsl:call-template>
  </xsl:variable>

  <mal:link xref="{$linkid}" depth="{$depth}"/>

  <xsl:variable name="next" select="$node/mal:info/mal:link[@type = 'next']"/>
  <xsl:if test="$next">
    <xsl:variable name="linklinkid">
      <xsl:call-template name="mal.link.xref.linkid">
        <xsl:with-param name="node" select="$next"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:if test="$linklinkid != '' and not(contains($done, concat(' ', $linklinkid, ' ')))">
      <xsl:variable name="nextnode" select="key('mal.cache.key', $linklinkid)"/>
      <xsl:call-template name="_mal.sort.tsort.node">
        <xsl:with-param name="node" select="$nextnode"/>
        <xsl:with-param name="done" select="concat($done, ' ', $linkid, ' ')"/>
        <xsl:with-param name="depth" select="$depth + 1"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:if>

  <xsl:variable name="page" select="document($node/@cache:href)"/>

  <xsl:variable name="topics">
    <xsl:for-each select="$node | $node//mal:section">
      <xsl:variable name="positionsort" select="position()"/>
      <xsl:variable name="groups">
        <xsl:choose>
          <xsl:when test="self::mal:page">
            <xsl:call-template name="_mal.sort.getgroups">
              <xsl:with-param name="node" select="$page/mal:page"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:otherwise>
            <xsl:variable name="sectid">
              <xsl:value-of select="substring-after(@id, '#')"/>
            </xsl:variable>
            <xsl:call-template name="_mal.sort.getgroups">
              <xsl:with-param name="node" select="$page//mal:section[@id = $sectid]"/>
            </xsl:call-template>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>
      <xsl:variable name="subtopics">
        <xsl:call-template name="mal.link.topiclinks">
          <xsl:with-param name="node" select="."/>
          <xsl:with-param name="groups" select="$groups"/>
        </xsl:call-template>
      </xsl:variable>
      <xsl:for-each select="exsl:node-set($subtopics)/*">
        <xsl:copy>
          <xsl:attribute name="positionsort">
            <xsl:value-of select="$positionsort"/>
          </xsl:attribute>
          <xsl:for-each select="@* | node()">
            <xsl:copy-of select="."/>
          </xsl:for-each>
        </xsl:copy>
      </xsl:for-each>
    </xsl:for-each>
  </xsl:variable>
  <xsl:variable name="topicnodes" select="exsl:node-set($topics)/*"/>
  <xsl:variable name="newdone">
    <xsl:value-of select="$done"/>
    <xsl:for-each select="$topicnodes">
      <xsl:variable name="linklinkid">
        <xsl:call-template name="mal.link.xref.linkid"/>
      </xsl:variable>
      <xsl:if test="$linklinkid != ''">
        <xsl:value-of select="concat(' ', $linklinkid)"/>
      </xsl:if>
    </xsl:for-each>
    <xsl:text> </xsl:text>
  </xsl:variable>
  <xsl:for-each select="$topicnodes">
    <xsl:sort data-type="number" select="@positionsort"/>
    <xsl:sort data-type="number" select="@groupsort"/>
    <xsl:sort select="mal:title[@type = 'sort']"/>
    <xsl:variable name="linklinkid">
      <xsl:call-template name="mal.link.xref.linkid"/>
    </xsl:variable>
    <xsl:if test="$linklinkid != '' and not(contains($done, concat(' ', $linklinkid, ' ')))">
      <xsl:for-each select="$mal.cache">
      <xsl:variable name="topic" select="key('mal.cache.key', $linklinkid)"/>
      <xsl:if test="$topic">
        <xsl:call-template name="_mal.sort.tsort.node">
          <xsl:with-param name="node" select="$topic"/>
          <xsl:with-param name="done" select="$newdone"/>
          <xsl:with-param name="depth" select="$depth + 1"/>
        </xsl:call-template>
      </xsl:if>
      </xsl:for-each>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<!--#* _mal.sort.getgroups -->
<xsl:template name="_mal.sort.getgroups">
  <xsl:param name="node" select="."/>
  <xsl:variable name="groups">
    <xsl:text> </xsl:text>
    <xsl:choose>
      <xsl:when test="$node/mal:links[@type = 'topic']">
        <xsl:for-each select="$node/mal:links[@type = 'topic']">
          <xsl:text> </xsl:text>
          <xsl:value-of select="@groups"/>
        </xsl:for-each>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$node/@groups"/>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text> </xsl:text>
  </xsl:variable>
  <xsl:if test="not(contains($groups, ' #first '))">
    <xsl:text> #first </xsl:text>
  </xsl:if>
  <xsl:value-of select="$groups"/>
  <xsl:if test="not(contains($groups, ' #default '))">
    <xsl:text> #default </xsl:text>
  </xsl:if>
  <xsl:if test="not(contains($groups, ' #last '))">
    <xsl:text> #last </xsl:text>
  </xsl:if>
</xsl:template>


</xsl:stylesheet>
